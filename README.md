## Project contains modules for automation

### API Framework
#### Tests for CRUD operations for user API
* To run tests via terminal navigate to the directory `api-framework` and run the command `mvn clean surefire-report:report`
```
cd api-framework
mvn clean surefire-report:report
```
* Report can be found in `api-framework/target/site/surefire-report.html`

### UI Framework
#### Contains 2 tests for login
* It is possible to run the tests in browsers: Chrome, Firefox.
* To run tests via terminal navigate to the directory `ui-framework` and run the command `mvn clean surefire-report:report -Dbrowser=firefox`
In case the browser was not specified, the tests will be run in Chrome.
```
cd ui-framework
mvn clean surefire-report:report
```
* Report can be found in `ui-framework/target/site/surefire-report.html`