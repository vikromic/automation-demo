package com.automation.demo.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO implements Serializable {

    private Long id;

    private String name;
    private String email;

    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
