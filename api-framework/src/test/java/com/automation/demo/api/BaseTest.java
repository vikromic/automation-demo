package com.automation.demo.api;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import com.automation.demo.model.ResponseData;
import com.automation.demo.model.UserDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.SneakyThrows;

public class BaseTest {

    private static final String BASE_URL = "https://reqres.in/api";
    private static final String USER_API = BASE_URL + "/users";
    private static final HttpClient client = HttpClient.newHttpClient();

    protected static final ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    @SneakyThrows
    public static String sendPOST(String url, String body) {
        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .header("Content-Type", "application/json")
                .uri(URI.create(url))
                .build();

        return client.send(request, HttpResponse.BodyHandlers.ofString()).body();
    }

    @SneakyThrows
    public static String sendGET(String url) {
        HttpRequest request = HttpRequest.newBuilder(URI.create(url)).GET()
                .header("Content-Type", "application/json")
                .build();

        return client.send(request, HttpResponse.BodyHandlers.ofString()).body();
    }

    @SneakyThrows
    public static String sendPUT(String url, String body) {
        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString(body))
                .header("Content-Type", "application/json")
                .uri(URI.create(url))
                .build();

        return client.send(request, HttpResponse.BodyHandlers.ofString()).body();
    }

    @SneakyThrows
    public static int sendDELETE(String url) {
        HttpRequest request = HttpRequest.newBuilder(URI.create(url)).DELETE().build();

        return client.send(request, HttpResponse.BodyHandlers.ofString()).statusCode();
    }

    @SneakyThrows
    public static UserDTO createUser(UserDTO user) {
        String body = sendPOST(USER_API, mapper.writeValueAsString(user));

        return mapper.readValue(body, UserDTO.class);
    }

    @SneakyThrows
    public static UserDTO readSingleUser(long id) {
        String body = sendGET(USER_API + "/" + id);

        return mapper.readValue(body, ResponseData.class).getData();
    }

    @SneakyThrows
    public static UserDTO updateUser(long id, UserDTO user) {
        String body = sendPUT(USER_API + "/" + id, mapper.writeValueAsString(user));

        return mapper.readValue(body, UserDTO.class);
    }

    @SneakyThrows
    public static int deleteUser(long id) {
        return sendDELETE(USER_API + "/" + id);
    }
}
