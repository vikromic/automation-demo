package com.automation.demo.api;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.automation.demo.model.UserDTO;

public class UserAPITests extends BaseTest {

    private static final int EXISTED_USER_ID = 12;

    private UserDTO testUser;

    @Before
    public void setUp() {
        testUser = new UserDTO();
        testUser.setName("user for testing");
        testUser.setEmail("aqa-test@fakegmail.com");
    }

    @Test
    public void create_user() {
        UserDTO createdUser = createUser(testUser);

        assertTrue(createdUser.getId() > 0);
        assertEquals(testUser.getName(), createdUser.getName());
        assertEquals(testUser.getEmail(), createdUser.getEmail());
    }

    @Test
    public void read_test_user() {
        UserDTO userDTO = readSingleUser(EXISTED_USER_ID);

        assertNotNull(userDTO.getEmail());
        assertFalse(userDTO.getEmail().isBlank());
    }

    @Test
    public void update_test_user() {
        UserDTO updatedUser = updateUser(EXISTED_USER_ID, testUser);

        assertNotNull(updatedUser.getUpdatedAt());
    }

    @Test
    public void delete_test_user() {
        int responseCode = deleteUser(EXISTED_USER_ID);

        assertEquals(204, responseCode);
    }
}
