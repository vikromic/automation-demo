package com.automation.demo.ui.model;

import org.openqa.selenium.WebDriver;

import lombok.Getter;

@Getter
public abstract class AbstractPage extends PageObject {

    private final Header header;

    protected AbstractPage(WebDriver driver) {
        super(driver);
        this.header = new Header(driver);
    }

    public void navigate() {
        getDriver().get(getUrl());
    }

    public abstract String getUrl();
}
