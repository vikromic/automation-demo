package com.automation.demo.ui.model;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AuthenticationPage extends AbstractPage {

    @FindBy(id = "email")
    private WebElement emailInput;

    @FindBy(id = "passwd")
    private WebElement passwordInput;

    @FindBy(id = "SubmitLogin")
    private WebElement submitButton;

    @FindBy(xpath = "//*[contains(text(),'Forgot your password?')]")
    private WebElement forgotPasswordButton;

    public AuthenticationPage(WebDriver driver) {
        super(driver);

        PageFactory.initElements(driver, this);
    }

    @Override
    public String getUrl() {
        return "http://automationpractice.com/index.php?controller=authentication";
    }

    public void login(String login, String password) {
        emailInput.sendKeys(login);
        passwordInput.sendKeys(password);
        submitButton.click();
    }
}
