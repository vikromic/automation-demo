package com.automation.demo.ui.model;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Header extends PageObject {

    @FindBy(xpath = "//*[contains(@class, 'login')]")
    private WebElement loginButton;

    @FindBy(xpath = "//*[contains(@class, 'logout')]")
    private WebElement logoutButton;

    public Header(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void clickLogin() {
        loginButton.click();
    }

    public void clickLogout() {
        logoutButton.click();
    }
}
