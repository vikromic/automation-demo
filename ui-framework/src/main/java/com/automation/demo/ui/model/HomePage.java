package com.automation.demo.ui.model;

import org.openqa.selenium.WebDriver;

public class HomePage extends AbstractPage {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    @Override
    public String getUrl() {
        return "http://automationpractice.com/";
    }
}
