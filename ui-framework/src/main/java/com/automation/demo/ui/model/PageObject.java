package com.automation.demo.ui.model;

import org.openqa.selenium.WebDriver;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class PageObject {

    private final WebDriver driver;
}
