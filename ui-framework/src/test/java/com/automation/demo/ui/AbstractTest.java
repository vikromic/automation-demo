package com.automation.demo.ui;

import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;

import com.automation.demo.ui.driver.DriverFactory;

public class AbstractTest {

    private static final String CONFIG_FILE = "config.properties";

    protected static Properties properties;

    protected WebDriver driver;

    @BeforeClass
    public static void beforeClass() throws Exception {
        properties = new Properties();
        properties.load(AbstractTest.class.getClassLoader().getResourceAsStream(CONFIG_FILE));
    }

    @Before
    public void setUp() {
        String browser = System.getProperty("browser", DriverFactory.Browser.CHROME.name()).toUpperCase();

        driver = DriverFactory.createDriver(DriverFactory.Browser.valueOf(browser));
    }

    @After
    public void tearDown() {
        driver.close();
    }
}
