package com.automation.demo.ui;

import org.junit.Before;
import org.junit.Test;

import com.automation.demo.ui.model.AuthenticationPage;
import com.automation.demo.ui.model.Header;
import com.automation.demo.ui.model.HomePage;

public class LoginSpec extends AbstractTest {
    private static final String INVALID_PASSWORD = "invalid";

    private HomePage homePage;
    private AuthenticationPage authenticationPage;

    private String validLogin;
    private String validPassword;


    @Before
    public void setUp() {
        super.setUp();

        validLogin = properties.getProperty("login");
        validPassword = properties.getProperty("password");

        homePage = new HomePage(driver);
        authenticationPage = new AuthenticationPage(driver);
    }

    @Test
    public void test_login_success_and_logout() {
        homePage.navigate();
        homePage.getHeader().clickLogin();

        authenticationPage.login(validLogin, validPassword);

        homePage.getHeader().clickLogout();
    }

    @Test
    public void test_login_failure() {
        homePage.navigate();
        homePage.getHeader().clickLogin();

        authenticationPage.login(validLogin, INVALID_PASSWORD);
    }
}
